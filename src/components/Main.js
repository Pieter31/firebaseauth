import React, { Component } from 'react';
import { StyleSheet, Platform, Image, Text, View,Button } from 'react-native';
import auth,{firebase} from '@react-native-firebase/auth';

class Main extends Component{
    state = { currentUser: null }

    componentDidMount() {
      const { currentUser } = firebase.auth()
      this.setState({ currentUser })}

    handleLogout = () => {
      firebase
      .auth()
      .signOut()
      .then(() => this.props.navigation.navigate('Loading'))
      .catch(error => this.setState({ errorMessage: error.message }));
      };

    render() {
        const { currentUser } = this.state
    return (

          <View style={styles.container}>

            <Text>
              Hi {currentUser && currentUser.email}!
            </Text>

            <Button title="Sign Out" onPress={this.handleLogout} />

          </View>

        )
      }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    }
})

export default Main;